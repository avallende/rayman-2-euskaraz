# Rayman 2 euskaraz

Proiektu hau hasi dut Rayman 2 jokoa euskarara itzultzeko

## Behar izandakoa

### Hizkuntza aldatzeko

- [Jolasaren GOG-eko bertsioa](https://www.gog.com/en/game/rayman_2_the_great_escape) instalatuta
- Fix.sna fitxategia

### Beste hizkuntza berri batera itzultzeko edo euskarazkoa aldatzeko

- MixerX-en [SNA Text Tool / RaymanTextTool](http://www.lokalizacie.sk/files/apps/RaymanTextTool.zip)
- Fix.sna edo Fix.txt fitxategia

## Tutorialak

### Nola bihurtu jokoaren SNA fitxategia irakurtzeko gai izango garen TXT fitxategi batean

- Ireki `Komandoaren gonbita` (Símbolo del sistema)
- Idatzi: `sna_nochar.exe -e Fix.sna Fix.txt`
  - `Fix.sna`: jokoaren fitxategi originala, iturria
  - `Fix.txt`: zein fitxategian gorde nahi dituzun 

### Nola bihurtu TXT fitxategia SNA fitxategi batean, jolasak uler dezan

- Ireki `Komandoaren gonbita` (Símbolo del sistema)
- Idatzi: `sna_nochar.exe -i Fix.sna Fix.txt`
  - `Fix.sna`: jokoari emango diogun fitxategia, itzulpenarekin
  - `Fix.txt`: irakurtzeko gai dagoen fitxategia, jokoarentzako bihurketa egin baino lehen

### Kontuan izateko

- Fitxategia irekitzean ta gordetzean, formatua `Windows-1252` izan beharko da. Bestela, arazoak emango ditu bihurketan.